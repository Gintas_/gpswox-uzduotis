package gintasapps.com.uzduotis;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager
{
    public static PreferenceManager instance;

    public static PreferenceManager getInstance(Context context)
    {
        if(instance == null)
            instance = new PreferenceManager(context);
        return instance;
    }

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;
    public PreferenceManager(Context context)
    {
        this.sharedPreferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        this.sharedPreferencesEditor = context.getSharedPreferences("preferences", Context.MODE_PRIVATE).edit();
    }

    public boolean isGoogleMapMaterialIconsEnabled()
    {
        return sharedPreferences.getBoolean("materialIcons", false);
    }

    public void setGoogleMapMaterialIconsEnabled(boolean isEnabled)
    {
        sharedPreferencesEditor.putBoolean("materialIcons", isEnabled);
        sharedPreferencesEditor.apply();
    }
}
