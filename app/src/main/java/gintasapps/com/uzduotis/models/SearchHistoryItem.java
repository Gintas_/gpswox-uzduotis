package gintasapps.com.uzduotis.models;

public class SearchHistoryItem
{
    public String word;
    public String partOfSpeech;
    public long searchTimestamp;

    @Override
    public String toString() {
        return word;
    }
}
