package gintasapps.com.uzduotis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import gintasapps.com.uzduotis.models.SearchHistoryItem;

class SearchSuggestionsAdapter extends ArrayAdapter<SearchHistoryItem>
{
    private ArrayList<SearchHistoryItem> suggestions = new ArrayList<>();
    private ArrayList<SearchHistoryItem> fullList = new ArrayList<>();
    private SearchFilter filter = new SearchFilter();
    public SearchSuggestionsAdapter(Context context, int resource, ArrayList<SearchHistoryItem> objects)
    {
        super(context, resource, objects);
        this.fullList = objects;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public SearchHistoryItem getItem(int position) {
        return suggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup)
    {
        SearchHistoryItem item = getItem(i);
        if(convertView == null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.search_suggestion_list_item, viewGroup, false);
        }
        TextView wordTextView = convertView.findViewById(R.id.wordTextView);
        TextView partOfSpeechTextView = convertView.findViewById(R.id.partOfSpeechTextView);
        TextView searchTimestampTextView = convertView.findViewById(R.id.searchTimestampTextView);

        wordTextView.setText(item.word);
        partOfSpeechTextView.setText(item.partOfSpeech);
        searchTimestampTextView.setText(new SimpleDateFormat("MM-DD HH:SS").format(new Date(item.searchTimestamp)));
        return convertView;
    }

    private class SearchFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence)
        {
            suggestions.clear();
            for(int i = 0; i < fullList.size(); i++)
            {
                if(fullList.get(i).word.toLowerCase().contains(charSequence.toString().toLowerCase()))
                    suggestions.add(fullList.get(i));
            }
            FilterResults results = new FilterResults();
            results.values = suggestions;
            results.count = suggestions.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
