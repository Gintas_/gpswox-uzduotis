package gintasapps.com.uzduotis;

import java.io.IOException;

import gintasapps.com.uzduotis.network.wordsapi.WordsApiService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UzduotisApplication extends android.app.Application
{
    public static WordsApiService wordsApiService;
    @Override
    public void onCreate()
    {
        super.onCreate();

        // init words api
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("X-Mashape-Key", getString(R.string.wordsapi_apikey)).build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://wordsapiv1.p.mashape.com/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        wordsApiService = retrofit.create(WordsApiService.class);

    }
}
