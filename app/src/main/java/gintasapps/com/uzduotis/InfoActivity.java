package gintasapps.com.uzduotis;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Random;

import gintasapps.com.uzduotis.network.wordsapi.WordsApiResponse;
import gintasapps.com.uzduotis.network.wordsapi.WordsApiResponseResult;

public class InfoActivity extends AppCompatActivity implements OnMapReadyCallback
{
    private GoogleMap googleMap;
    private static final int[] mapMaterialIconsResIds = { R.drawable.ic_icon_accessibility, R.drawable.ic_icon_build, R.drawable.ic_icon_face, R.drawable.ic_icon_favorite, R.drawable.ic_icon_flight_takeoff };
    private ArrayList<Marker> mapMarkers = new ArrayList<>();
    private WordsApiResponse wordsApiResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wordsApiResponse = new Gson().fromJson(getIntent().getStringExtra("wordsApiResponse"), WordsApiResponse.class);

        // set top content
        TextView contentTextView = findViewById(R.id.contentTextView);
        String content = formatContentString(true, wordsApiResponse);
        contentTextView.setText(Html.fromHtml(content));

        // set bottom content
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap)
    {
        this.googleMap = googleMap;

        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng)
            {
                MarkerOptions markerOptions = new MarkerOptions().position(latLng);
                if(PreferenceManager.getInstance(getBaseContext()).isGoogleMapMaterialIconsEnabled())
                {
                    int iconResId = mapMaterialIconsResIds[new Random().nextInt(mapMaterialIconsResIds.length)];
                    markerOptions.icon(Utils.bitmapDescriptorFromVector(InfoActivity.this, iconResId));
                }

                Marker marker = googleMap.addMarker(markerOptions);
                mapMarkers.add(marker);
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter()
        {
            @Override
            public View getInfoWindow(Marker marker)
            {
                View view = getLayoutInflater().inflate(R.layout.infowindow_layout, null);

                TextView contentTextView = view.findViewById(R.id.contentTextView);
                String contentText = formatContentString(false, wordsApiResponse);

                contentTextView.setText(Html.fromHtml(contentText));

                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

    }

    private String formatContentString(boolean addExamples, WordsApiResponse wordsApiResponse)
    {
        String content = "";
        content += "<b>" + wordsApiResponse.word + "</b>" + "<br>";
        for(int i = 0; i < wordsApiResponse.results.size(); i++)
        {
            WordsApiResponseResult item = wordsApiResponse.results.get(i);
            content += "<i>" + (i + 1) + ".</i> " + item.partOfSpeech + ", " + item.definition;

            if(addExamples && item.examples != null && item.examples.size() > 0)
            {
                content += ", examples: ";
                for(int j = 0; j < item.examples.size(); j++)
                {
                    content += item.examples.get(j);
                    if(j < item.examples.size() - 1)
                        content += "; ";
                }
            }

            if(i < wordsApiResponse.results.size() - 1)
                content += "<br>";
        }
        return content;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        else if(item.getItemId() == R.id.menu_option_defaultMapIcons)
        {
            PreferenceManager.getInstance(this).setGoogleMapMaterialIconsEnabled(false);
            resetIconsOfMapMarkers();
        }
        else if(item.getItemId() == R.id.menu_option_materialMapIcons)
        {
            PreferenceManager.getInstance(this).setGoogleMapMaterialIconsEnabled(true);
            resetIconsOfMapMarkers();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.info_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void resetIconsOfMapMarkers()
    {
        for(Marker marker : mapMarkers)
        {
            if(PreferenceManager.getInstance(this).isGoogleMapMaterialIconsEnabled())
            {
                int iconResId = mapMaterialIconsResIds[new Random().nextInt(mapMaterialIconsResIds.length)];
                marker.setIcon(Utils.bitmapDescriptorFromVector(InfoActivity.this, iconResId));
            }
            else
            {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker());
            }
        }
    }
}
