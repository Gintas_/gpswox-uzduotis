package gintasapps.com.uzduotis.network.wordsapi;

import java.util.ArrayList;

public class WordsApiResponse
{
    // unused response values omitted
    public String word;
    public ArrayList<WordsApiResponseResult> results;
}
