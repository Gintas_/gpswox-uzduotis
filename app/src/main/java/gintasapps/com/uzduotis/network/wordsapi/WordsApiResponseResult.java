package gintasapps.com.uzduotis.network.wordsapi;

import java.util.ArrayList;

public class WordsApiResponseResult
{
    public String definition, partOfSpeech;
    public ArrayList<String> synonyms, typeOf, derivation, examples;
}
