package gintasapps.com.uzduotis.network.wordsapi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WordsApiService
{
    @GET("words/{word}")
    Call<WordsApiResponse> lookup(@Path("word") String word);

}
