package gintasapps.com.uzduotis;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import gintasapps.com.uzduotis.models.SearchHistoryItem;

public class SearchHistoryManager
{
    public static SearchHistoryManager instance;

    public static SearchHistoryManager getInstance(Context context)
    {
        if(instance == null)
            instance = new SearchHistoryManager(context);
        return instance;
    }

    private ArrayList<SearchHistoryItem> historyItems;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;
    public SearchHistoryManager(Context context)
    {
        this.sharedPreferences = context.getSharedPreferences("history_items", Context.MODE_PRIVATE);
        this.sharedPreferencesEditor = context.getSharedPreferences("history_items", Context.MODE_PRIVATE).edit();

        loadHistory();
    }

    private void loadHistory()
    {
        historyItems = new Gson().fromJson(sharedPreferences.getString("historyItems", ""), new TypeToken<ArrayList<SearchHistoryItem>>() {}.getType());
        if(historyItems == null)
            historyItems = new ArrayList<>();

    }

    public void addToHistory(SearchHistoryItem item)
    {
        historyItems.add(item);
        try {
            sharedPreferencesEditor.putString("historyItems", new Gson().toJson(historyItems));
        } catch (Exception e) {
            e.printStackTrace();
        }
        sharedPreferencesEditor.apply();
    }

    public ArrayList<SearchHistoryItem> getHistoryItems()
    {
        return historyItems;
    }
}
