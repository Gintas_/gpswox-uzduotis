package gintasapps.com.uzduotis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import gintasapps.com.uzduotis.models.SearchHistoryItem;
import gintasapps.com.uzduotis.network.wordsapi.WordsApiResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
{
    private SearchSuggestionsAdapter searchSuggestionsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set up custom search action bar
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.search_action_bar, null);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // set up search functionality
        final AutoCompleteTextView searchInput = actionBarLayout.findViewById(R.id.searchInput);
        searchInput.setThreshold(1);
        ArrayList<SearchHistoryItem> searchHistoryItems = SearchHistoryManager.getInstance(this).getHistoryItems();
        searchSuggestionsAdapter = new SearchSuggestionsAdapter(this, android.R.layout.select_dialog_item, searchHistoryItems);
        searchInput.setAdapter(searchSuggestionsAdapter);
        // start search once enter is clicked
        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                {
                    performWordSearch(searchInput.getText().toString());
                }
                return true;
            }
        });

        // start search once item from suggestions list is selected
        searchInput.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                performWordSearch(searchSuggestionsAdapter.getItem(i).word);
            }
        });
    }

    private void performWordSearch(final String word)
    {
        UzduotisApplication.wordsApiService.lookup(word).enqueue(new Callback<WordsApiResponse>() {
            @Override
            public void onResponse(Call<WordsApiResponse> call, Response<WordsApiResponse> response)
            {
                // add word to search history
                SearchHistoryItem searchHistoryItem = new SearchHistoryItem();
                searchHistoryItem.word = word;
                searchHistoryItem.searchTimestamp = System.currentTimeMillis();
                if(response.body().results.size() > 0)
                    searchHistoryItem.partOfSpeech = response.body().results.get(0).partOfSpeech;
                else
                    searchHistoryItem.partOfSpeech = "";
                SearchHistoryManager.getInstance(MainActivity.this).addToHistory(searchHistoryItem);

                Intent i = new Intent(MainActivity.this, InfoActivity.class);
                i.putExtra("wordsApiResponse", new Gson().toJson(response.body()));
                startActivity(i);
            }

            @Override
            public void onFailure(Call<WordsApiResponse> call, Throwable t)
            {
                Toast.makeText(MainActivity.this, getString(R.string.networkError), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
